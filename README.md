# Dictionary

Dictionary create one collection of variables while you program so we can translate our application in real time and also create on interface for users to edit those values inside the collection

## Usage
You have to simple call the dictionary inside your application or inside the templates

inside the js code you do it like this:   dictionary('Hello');
inside the template you do it like this:  {{dictionary 'Hello'}}

## What happen after that.
* First we will add this variable inside the default language called Hello and we will also bind it to the template we call it.
* After this, when we change to other language, we will translate with google api and include other document with translation of that variable.
* So, everything we do will be treated as variable, so we do not need to build json for translation anymore.

## License
This project is licensed under MIT.
