Template.dictionary_list.onCreated(function(){
  var _this = this;
  this.autorun(function(){
      _this.subscribe('dictionary',{ search: {
                                               l: Session.get('search_dictionary_language'),
                                               t: Session.get('search_dictionary_template')
                                             },
                                     pag_display: null
                                   });
      _this.subscribe('dictionaryLanguages');
      _this.subscribe('dictionaryTemplates');
  });
});

Template.dictionary_list.helpers({
  'filteredDictionary': function(){
    var language = Session.get('search_dictionary_language');
    var template = Session.get('search_dictionary_template');
    var keyword  = Session.get('search_dictionary_keyword');
    var options = {};
    if(language){
      options.l = language;
    }
    if(template){
      options.t = template;
    }
    if(keyword){
      var search =  new RegExp(keyword,'i')
      options.$or = [{t: search}, {v: search}, {d: search}];
    }
    return Dictionary.find(options);
  },
  'countDictionary': function(){
    var language = Session.get('search_dictionary_language');
    var template = Session.get('search_dictionary_template');
    var keyword  = Session.get('search_dictionary_keyword');
    var options = {};
    if(language){
      options.l = language;
    }
    if(template){
      options.t = template;
    }
    if(keyword){
      var search =  new RegExp(keyword,'i')
      options.$or = [{t: search}, {v: search}, {d: search}];
    }
    return Dictionary.find(options).count();
  },
  'dictionaryTemplates': function(){
    //console.log(DictionaryTemplates.find({}).count());
    return DictionaryTemplates.find({});
  },
  'dictionaryLanguages':function(){
    return DictionaryLanguages.find({});
  }
});


var typingTimer = null;
function updateWhenStopTyping(document, event) {
	 Meteor.clearTimeout(typingTimer);
   typingTimer = Meteor.setTimeout(function() {
                                                 var doc = {
                                                       d: event.target.value
                                                 }
                                                 Meteor.call('dictionary.update',document._id, doc)
																							}, 1000);
}

Template.dictionary_list.events({
    'keyup .liveupdate': function(event) {
        updateWhenStopTyping(this, event);
    }
});
