Dictionary = new Mongo.Collection('dictionary');
DictionaryTemplates = new Mongo.Collection('dictionary_templates');
DictionaryLanguages = new Mongo.Collection('dictionary_languages');

Dictionary.allow({
	insert: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
	update: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
	remove: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
});

DictionaryTemplates.allow({
	insert: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
	update: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
	remove: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
});

DictionaryLanguages.allow({
	insert: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
	update: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
	remove: function(userId, user, fields, modifier) {
		// TODO: security
		return true;
	},
});
