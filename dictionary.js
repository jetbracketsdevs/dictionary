// Write your package code here!

// Variables exported by this module can be imported by other packages and
// applications. See dictionary-tests.js for an example of importing.
export const name = 'dictionary';

dictionary_options = function dictionary_options(){
    if(Meteor.settings.public && Meteor.settings.public.dictionary){
        var settings = Meteor.settings.public.dictionary;
    }else{
        var settings = {};
    }
    var options = _.extend(
        {
            defaultLanguage: "en",
            userDefaultLanguage: "en",
            debug: false
        },
        settings);
        return options;
    }

    dictionary_debug = function dictionary_debug(log){
        if(dictionary_options().debug == true){
            console.log('Dictionary: '+log);
        }
    }

    dictionary_curLanguage = function dictionary_curLanguage(){
        var options = dictionary_options();
        var curLanguage;
        if(Meteor.user()){
            if(!UserSession.get("dictionary.curLanguage")){
                //user is logged, but dont have the language set, so we set the language to userDefaultLanguage
                //dictionary_debug('Dictionary: user is logged, but dont have the language set, so we set the language to userDefaultLanguage = '+options.userDefaultLanguage);
                UserSession.set("dictionary.curLanguage", options.userDefaultLanguage);
                curLanguage = options.userDefaultLanguage;
            }else{
                //user is logged and have the language for it, so we just load his language.
                //dictionary_debug('Dictionary: user is logged and have the language for it, so we just load his language = '+ UserSession.get("Dictionary_curLanguage"));
                curLanguage = UserSession.get("dictionary.curLanguage");
            }
        }else{
            //if user is not logged we set user default language to defaults or the options set on settings
            //dictionary_debug('if user is not logged we set user default language to defaults or the options set on settings = '+options.userDefaultLanguage);
            curLanguage = options.userDefaultLanguage;
        }
        return curLanguage;
    }

    dictionary = function dictionary(curVariable) {
        var curTemplate = Template.instance().view.name.replace('Template.','');
        if(Session.get('dictionary.ready.'+curTemplate) == true){
            var options = dictionary_options();
            var curLanguage = dictionary_curLanguage();
            if(!curVariable){ //dont translate junk variables
                dictionary_debug('Variable in dictionary is junk variable. check Template:'+curTemplate+' Language:'+curLanguage+' Variable:'+curVariable);
                return '';
            }
            //dictionary_debug('curLanguage: '+curLanguage+' curTemplate:'+curTemplate+' curVariable:'+curVariable);
            var result = Dictionary.findOne({
                l: curLanguage,
                t: curTemplate,
                v: curVariable
            });
            if(result){
                return result.d;
            }else{
                //We will only add if the translation is not found and data is ready
                var _array = Session.get('dictionary.translate');
                //this function below prevents the client to call again because it take some time to translate it so
                //we will only call to translate once per section.
                if (_.findWhere(_array, curLanguage+curTemplate+curVariable) == null) {
                    if(_array){
                        _array.push(curLanguage+curTemplate+curVariable);
                    }else{
                        _array = [];
                    }
                    Session.set('dictionary.translate', _array);
                    console.log('calling meteor call to add new variable to dictionary on Template:'+curTemplate+' Language:'+curLanguage+' Variable:'+curVariable);
                    Meteor.call('dictionary.insert', {l: curLanguage, t: curTemplate, v: curVariable, dl: dictionary_options().defaultLanguage });
                }
            }
        }else{
            return '';
        }
    }

    Meteor.startup(function(){
        for(var property in Template){
            // check if the property is actually a blaze template
            var template=Template[property];
            if(Blaze.isTemplate(template)){
                //console.log(template.viewName);
                // assign the template an onRendered callback who simply prints the view name
                template.helpers({
                    'dictionary': function(curVariable){
                        var _template = Template.instance().view.name.replace('Template.','');
                        var result = Session.get('dictionary.ready.'+_template);
                        if(result == true){
                            return dictionary(curVariable);
                        }else{
                            return '';
                        }
                    }
                });
                template.onCreated(function(){
                    var _template = this.view.name.replace('Template.','');
                    var _array = Session.get('dictionary.templates');
                    if (_.findWhere(_array, _template) == null) {
                        if(_array){
                            _array.push(_template);
                        }else{
                            _array = [];
                        }
                        Session.set('dictionary.templates', _array);
                        Session.set('dictionary.ready.'+_template, false);
                        Meteor.subscribe('dictionary', {
                            filters: {
                                l: dictionary_curLanguage(),
                                t: _template
                            }
                        }, function(){
                            //checking for subscriptionsReady is not sufficient to know if the data is ready
                            //the subscription can be unpopulated or on the process if we do not do it this way.
                            Session.set('dictionary.ready.'+_template, true);
                        });
                    }
                });
            }
        }
    });

    Template.registerHelper('dictionaryLanguage', function (curImg) {
        return dictionary_curLanguage();
    });

    Template.registerHelper('dictionaryCurFlag', function () {
        var curLanguage = dictionary_curLanguage();
        switch(curLanguage){
            case 'pt':
            curFlag = 'br';
            break;
            default:
            curFlag = 'us';
            break;
        }
        return curFlag;
    });
