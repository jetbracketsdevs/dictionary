Package.describe({
  name: 'jetbrackets:dictionary',
  version: '0.0.5',
  // Brief, one-line summary of the package.
  summary: 'Translation Package to add multilanguage support.',
  // URL to the Git repository containing the source code for this package.
  git: 'https://bitbucket.org/jetbracketsdevs/dictionary',
  // By default, Meteor will default to using README.md for documentation.
  // To avoid submitting documentation, set this field to null.
  documentation: 'README.md'
});

Package.onUse(function(api) {
  api.versionsFrom('1.4.4.3');
  api.use('ecmascript');
  api.mainModule('dictionary.js', 'client');
  api.mainModule('server.js', 'server');
  api.addFiles(['collection.js']);
  api.addFiles(['client/startup.js'], 'client');
  api.addFiles(['server/startup.js'], 'server');
  api.addFiles(['templates/dictionary.html','templates/dictionary_list.html','templates/dictionary_list.js'], 'client');
  api.use(['tracker','mongo','session', 'blaze-html-templates@1.1.2']);
  api.export('dictionary', 'client');
});

Package.onTest(function(api) {
  api.use('ecmascript');
  api.use('tinytest');
  api.use('jetbrackets:dictionary');
  api.mainModule('dictionary-tests.js');
});
