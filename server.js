Meteor.publish('dictionary', function(options) {
  if(options.limit){
    return Dictionary.find(options.filters, options.limit);
  }else{
    //console.log("dictionary count:" + Dictionary.find(options.filters).count());
    //console.log("dictionary", options.filters);
    return Dictionary.find(options.filters);
  }
});

Meteor.publish("dictionaryTemplates", function(argument){
  return DictionaryTemplates.find({});
});

Meteor.publish("dictionaryLanguages", function(argument){
  return DictionaryLanguages.find({});
});

Meteor.methods({
  'dictionary.insert': addDictionary,
  'dictionary.update': updateDictionary
});

function addDictionary(doc) {
  // {
  //   dl: -- Default Language,
  //   l:  -- Current Language,
  //   t:  -- Template,
  //   v:  -- Variable
  // }
  if(!DictionaryTemplates.findOne({value: doc.t})){
    DictionaryTemplates.insert({text: doc.t, value: doc.t});
  }
  if(!Dictionary.findOne({l: doc.l,t: doc.t,v: doc.v})){
    if(doc.dl != doc.l){ //if language is different than default we need to translate.
      if(Meteor.settings.public && Meteor.settings.public.dictionary && Meteor.settings.public.dictionary.googletranslateapikey){
        var url = 'https://translation.googleapis.com/language/translate/v2?key='+Meteor.settings.public.dictionary.googletranslateapikey+'&q='+doc.v+'&source='+doc.dl+'&target='+doc.l+'&format=text';
        url = url.replace('#', '');
        HTTP.call('GET', url,
        {}, function(error, response) {
          if (error) {
            //insert variable without translation because we could not translate.
            Dictionary.insert({
              l: doc.l,
              t: doc.t,
              v: doc.v,
              d: dov.v
            });
            console.log("Error Translating with google api inside dictionary.js: "+error);
          }else{
            Dictionary.insert({
              l: doc.l,
              t: doc.t,
              v: doc.v,
              d: response.data.data.translations[0].translatedText
            });
            console.log("New dictionary string was translated and added to database: Language:"+doc.l+", Template:"+doc.t+", Variable:"+doc.v);
          }
        });
      }else{
        console.log("No api key to translate so we add without translation: Language:"+doc.l+", Template:"+doc.t+", Variable:"+doc.v);
        Dictionary.insert({l: doc.l,t: doc.t,v: doc.v,d: dov.v});
      }
    }else{
      Dictionary.insert({l: doc.l,t: doc.t,v: doc.v,d: dov.v});
    }
  }
}

function updateDictionary(_id, doc) {
  Dictionary.update({_id: _id}, {$set: doc});
}
