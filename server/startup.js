Meteor.startup(function () {
  if (Dictionary.find({}).count() < 1) {
    Dictionary._ensureIndex({
       "l": 1,
       "t": 1,
       "v": 1
    }, {unique: true});
  }
  if (DictionaryTemplates.find({}).count() < 1) {
    DictionaryTemplates._ensureIndex({
       "v": 1
    }, {unique: true});
  }
  if (DictionaryLanguages.find({}).count() < 1) {
    DictionaryLanguages.insert({'text':'English', 'value':'en', 'flag':'us', 'enabled': 1});
    DictionaryLanguages.insert({'text':'Português', 'value':'pt', 'flag':'br', 'enabled': 1});
  }
});
